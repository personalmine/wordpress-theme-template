<header id="site-header">
  <?php the_custom_logo(); ?>
  <h1 id="header">
    <a href="<?php bloginfo('url'); ?>" class="site-title"><?php bloginfo('name'); ?></a>
  </h1>
</header>