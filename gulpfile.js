var gulp = require('gulp'),
  compass = require('gulp-compass');

gulp.task('compass', function() {
  gulp.src('./src/sass/*.scss')
    .pipe(compass({
      config_file: './config.rb',
      css: 'assets/css',
      sass: 'src/sass',
      image: 'assets/images'
    }))
    .on('error', function(error) {
      console.log(error);
      this.emit('end');
    })
    .pipe(gulp.dest('assets/css'));
});

gulp.task('default', function() {
  // place code for your default task here
  gulp.watch('src/sass/**/*.scss', ['compass']);
});