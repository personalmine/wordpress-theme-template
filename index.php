<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_get_document_title(); ?>
  <?php wp_head(); ?>
</head>
<body>
  <?php get_header(); ?>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>