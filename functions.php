<?php
function queueScripts() {
  $v = "1.0.1";
  if (!is_admin()) {
    wp_enqueue_script('app', get_template_directory_uri().'/assets/js/app.js', ['jquery'], $v, true);
  }
}

function queueStyles() {
  $v = "1.0.1";
  if (!is_admin()) {
    wp_enqueue_style('app', get_template_directory_uri().'/assets/css/app.css', false, $v, 'all');
  }
}
add_theme_support('custom-logo');
add_theme_support('title-tag');
add_theme_support('custom-header');
add_action('init', 'queueStyles');
add_action('init', 'queueScripts');